var http = require('http');
var fs = require('fs');
var url = require('url');
var request = require('request');
var express = require("express");
var cookieParser = require('cookie-parser');


var users;


var app = express();

app.use(cookieParser());


require('./mongo.js').init(function (err, _db) {
    if (err) throw err;
    db = _db;
    users = db.collection('move');
});

app.get('/', isLogged, function (req, res) {
    if (req.user) {
        res.redirect('/step');
        return;
    } else {
        res.writeHead(200, {'Content-Type': 'text/html;'});
        var data = '<a href="https://api.moves-app.com/oauth/v1/authorize?response_type=code&client_id=RQ70F4fm4RP3M470NEb1ePs14ZsbEm9O&scope=default activity">GoToHaveToken</a>';
        res.end(data);
    }
});

app.get('/step', isLogged, getInfo, function (req, res) {
    if (req.user && req.user.todaySteps) { 
        console.log(req.user);    
        var data = req.user.todaySteps + ' your today steps';
        res.end(data);
        return;
    }
    res.redirect('/');
    return;
});



app.get('/auth', function (req, res) {
    var path = url.parse(req.url, true);
    console.log(path.query + '   =   path')
    if (!path.query.code) {
        res.redirect('/')
        return;
    }
   
    var headers = {
        'User-Agent':       'Super Agent/0.0.1',
        'Content-Type':     'application/x-www-form-urlencoded'
    }

    var options = {
        url: 'https://api.moves-app.com/oauth/v1/access_token',
        method: 'POST',
        headers: headers,
        form: {'grant_type': 'authorization_code', 'code': path.query.code, 'client_id':'RQ70F4fm4RP3M470NEb1ePs14ZsbEm9O', 'client_secret':'43qAi7vWaC6oUShDtUMXyAW0mseuNVnypYqQ7DBV1Gb6ph7hxaJcYv8N2gsRUWg8'}
    }

    // Start the request
    request(options, function (error, response, body) {
        console.log('запрос выполнен');
        if (error) {
            console.log(error);
            res.send(500, "error");
            res.redirect('/');
            return;
        }
        var message = JSON.parse(body);
        console.log(message);
        res.cookie("id", message.access_token);

        users.findOne({user_id: message.user_id}, function (err, result) {

            if (result) {
                users.update({user_id: message.user_id}, {
                    $set: {
                        access_token: message.access_token,
                        token_type: message.token_type,
                        expires_in: message.expires_in,
                        refresh_token: message.refresh_token
                    }
                }, function (err, result) {
                    if (err) throw err;
                    console.log('я перезаписался');
                    res.redirect('/step');
                    return;
                })

            }
            
            users.insert(message, function (err, result) {
                if (err) throw err;
                console.log('я записался и получил токен');
                res.redirect('/step');
                return;
            })  
        })
    });
});

app.listen(8090);


function isLogged(req, res, next) {
    if (req.cookies.id) {
        users.findOne({access_token: req.cookies.id}, function (err, result) {
            if (result) {
                req.user = result;
            }
            else {
                req.user = null;
            }
            next();
        })
    } else {
        req.user = null;
        next();
    }

}


function getInfo(req, res, next) {
    if (!req.user) {
        next();
        return;
    }
    

    var d = new Date();
    var year = ('' + d.getFullYear()).slice(0);
    var month = ('0' + (d.getMonth() + 1)).slice(-2);
    var date = ('0' + d.getDate()).slice(-2);
    var allDate = ''+year + month + date;

    var path = "https://api.moves-app.com/api/1.1/user/summary/daily/" + allDate + "?access_token=" + req.user.access_token;
    request(path, function(error, response, body){
        if (error) {
            console.log('ошибка при получении шагов' + err);
            req.user = null;
            next();
            return;
        }

        var message = JSON.parse(body);
        var allSteps = getSteps(message[0]);
        
        console.log(req.user.user_id + 'ididid')
        users.findOne({user_id: req.user.user_id}, function (err, result) {
            if (result) {
                users.update({user_id: req.user.user_id}, {
                    $set: {
                        todaySteps: allSteps
                    }
                }, function (err, result) {
                    if (err) throw err;
                    next();
                    console.log('я перезаписался и добавил шаги');
                    return;  
                });  
            }
            else {
                req.user = null;
                next();
                return;
            }  
        });
    });
}


function getSteps(message) {
    
    var allSteps = 0;
    var i = 0;
    for (i; i < 3; i++) {
       if (message.summary && message.summary[i] && message.summary[i].steps){
            allSteps = allSteps + message.summary[i].steps;
        } 
    }
    console.log(allSteps+'steps!!!!!!!!!!!!!!');   
    return allSteps;
}

